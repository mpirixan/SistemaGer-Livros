# SistemaGer-Livros
Pequeno sistema de gerenciamento de Livro usando Spring e Angular para fixação de conhecimento em 
Java, Typescript, SQL, CI/CD, GitFlow.

### Changelog v1
1. Angular substituirá o Vue
2. Recomeço de projeto para práticas em Java e Angular
3. CI/CD em VM Linux Debian 11 com Jenkins(aprendizado) e Node.js - A ser feito
4. Build em Docker - A ser feito
5. Banco de Dados em Docker - A ser feito